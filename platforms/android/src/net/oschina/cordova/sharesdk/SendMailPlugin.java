package net.oschina.cordova.sharesdk;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.content.Intent;
import android.util.Log;


public class SendMailPlugin extends CordovaPlugin {
	private static final String TAG = "SendMailPlugin";
	private Context mContext;

	@Override
	public void initialize(CordovaInterface cordova, final CordovaWebView webView) {
		Log.v(TAG, "initialize");
		super.initialize(cordova, webView);
		mContext = cordova.getActivity();
		System.out.println("SendMailPlugin==initialize");

	    
	}

	@Override
	public boolean execute(String action, JSONArray args,
			CallbackContext callbackContext) throws JSONException {
		Log.v(TAG, "execute>>" + action + ",arg>>" + args.toString());
		System.out.println("SendMailPlugin");
		
		String str=args.getString(0);
		sendMailByIntent(str);

   
		
		return true;
	}
    public void sendMailByIntent(String str) {  
    	
    	System.out.println("sendMailByIntent");
        String[] reciver = new String[] { "oschina.net@gmail.com" };  //收件人的邮箱地址
//        String[] mySbuject = new String[] { "test" };
        String mySbuject ="开源中国-用户反馈";  //主题内容
        String myCc = str;//邮件内容  
        String mybody = str;  
        Intent myIntent = new Intent(android.content.Intent.ACTION_SEND);  
        myIntent.setType("plain/text");  
        myIntent.putExtra(android.content.Intent.EXTRA_EMAIL, reciver);  
        myIntent.putExtra(android.content.Intent.EXTRA_CC, myCc);  
        myIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, mySbuject);  
        myIntent.putExtra(android.content.Intent.EXTRA_TEXT, mybody);  
//        mContext.startActivity(Intent.createChooser(myIntent, "mail test"));
        cordova.getActivity().startActivityForResult(Intent.createChooser(myIntent, "mail test"), 15);  
  
  
    }
}

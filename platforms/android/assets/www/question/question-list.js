define(['text!question/question-list.html', "../base/openapi", '../base/util'],
	function(viewTemplate, OpenAPI, Util) {
		return Piece.View.extend({
			id: 'question_question-list',
			events: {
				"click .questionContent": "goToQuestionList",
				"click .editBtn": "editQuestion",
				"click .questionListImg": 'goToUserInfor'
			},
			goToUserInfor: function(imgEl) {
				Util.imgGoToUserInfor(imgEl);
				// imgEl.stopPropagation();// 标准W3C的取消冒泡   
				//          imgEl.preventDefault();// 取消默认行为 
			},
			goToQuestionList: function(el) {
				var $target = $(el.currentTarget);
				var id = $target.attr("data-id");
				var from = $('.active').attr('data-value').split('/')[1];
				//checkDtail  toggle comment-list or xx-detail
				var checkDetail = "question/question-detail";
				//type    add  favorite
				var type = 2;
				//comment list
				var com = 2;
				this.navigate("question-detail?id=" + id + "&from=" + from + "&checkDetail=" + checkDetail + "&fromType=" + type + "&com=" + com, {
					trigger: true
				});
			},
			editQuestion: function() {
				var from = $('.active').attr('data-value').split('/')[1];
				this.navigate("question-edit?type=1" + "&from=" + from, {
					trigger: true
				});
			},
			render: function() {
				$(this.el).html(viewTemplate);
				
				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				Util.loadList(this, 'question-question-list', OpenAPI.question_list, {
					'catalog': 1,
					'dataType': OpenAPI.dataType,
					'page': 1,
					'pageSize': OpenAPI.pageSize
				});
			}
		}); //view define

	});
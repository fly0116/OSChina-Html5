define(function(require) {
     var v1 = require('demo/index');
     var v2 = require('demo/login');
     return {
         'index': v1,
         'login': v2
      };
});